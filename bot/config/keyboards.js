const { Markup } = require('telegraf');

const buttons = {
  // Кнопки основной клавиатуры
  notes: { text: '📝 Заметки' },
  reminders: { text: '⏱️Напоминания' },
  setup: { text: '⚙️Управление' },
  help: { text: '❓ Помощь' },

  // Кнопки клавиатуры редактирования/управления записями
  setupNotes: { text: '⚙️Заметки' },
  setupReminders: { text: '⚙️Напоминания' },
  setupBack: { text: '🔙 Назад' },

  // Кнопки inline-клавиатуры при редактировании
  done: { text: '✅️Завершить', action: 'done' },
  undone: { text: '🔲 Отменить', action: 'undone' },
  delete: { text: '❌ Удалить', action: 'delete' },
};

const keyboards = {
  // Основная клавиатура
  default: Markup.keyboard([
    [buttons.notes.text, buttons.reminders.text],
    [buttons.setup.text, buttons.help.text],
  ]),
  // Клавиатура раздела редактирования
  setup: Markup.keyboard([
    [buttons.setupNotes.text, buttons.setupReminders.text],
    [buttons.setupBack.text],
  ]),
};

module.exports = {
  buttons,
  keyboards,
};