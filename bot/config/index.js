module.exports = {
  // Настройки бота
  telegram: {
    token: 'YOUR_BOT_TOKEN',
    botStartedAt: Math.floor(Date.now() / 1000),
  },
  // Настройки базы данных
  database: {
    uri: '[YOU_MONGO_CONNECTION_STRING]',
    options: {
      user: '[YOUR_MONGO_USER]',
      pass: '[YOUR_MONGO_PASSWORD]',
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  },
  // Разные переменные и параметры
  misc: {
    dateReminderInterval: 1000 * 60, // Интервал проверки напоминаний - раз в минуту
    dateReminderBulk: 20, // Не более 20 сообщений в минуту, итого не более 14400 в сутки
  },
};