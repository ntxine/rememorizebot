const moment = require('moment');

/**
 * Обработка простых сообщений
 * @param db
 * @param ctx
 * @returns {Promise<void>}
 */
module.exports = async function message(db, ctx) {
  // Регулярка для определения даты
  // DD-MM-YYYY, DD-MM-YY, разделители могут быть '.', '-', '/'
  // Честно позаимствовано на stackoverflow
  const regExpDate = new RegExp(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/i);
  const { message } = ctx.update;
  const { text } = message;

  // Разбираем сообщение по пробелам
  const parts = text.trim().split(' ');

  // Создаем новую запись (пока без записи)
  const record = new db.Record({
    telegramUserId: message.from.id,
    isPeriodic: false,
    text,
  });

  if (parts.length > 1 && regExpDate.test(parts[0])) {
    record.text = parts.slice(1).join(' ');
    // Если в первом слове дата, то сохраним запись как напоминание
    const dateFixed = parts[0].split(/\D/g).reverse().join('-');
    record.remindString = moment(dateFixed, 'YYYY-MM-DD').format('YYYY-MM-DD');
    // Сообщение о создании напоминания
    ctx.reply(`Я обязательно напомню ${record.remindString}`);
  } else {
    // В ином случае как простую заметку
    const replies = ['Я запомнил!', 'Я думаю, пригодится.', 'Если забудешь - я напомню!',];
    // Сообщение о создании заметки
    ctx.reply(replies[Math.floor(Math.random() * replies.length)]);
  }

  // Сохраняем запись
  await record.save();
};
