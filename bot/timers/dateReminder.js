const moment = require('moment');
const config = require('../config/index.js');

/**
 * Функция создает напоминание пользователю
 * @param bot
 * @param record
 * @returns {Promise<void|WriteOpResult|this>}
 */
async function remindUser(bot, record) {
  const header = '⏱⏱⏱ *НАПОМИНАНИЕ* ⏱⏱⏱\n';
  bot.telegram.sendMessage(
    record.telegramUserId,
    `${header}${record.text}`,
    { parse_mode: 'Markdown' }
  );
  record.isDone = true;
  return record.save();
}

/**
 * Функция проверки напоминаний
 * @param db
 * @param bot
 * @returns {Promise<void>}
 */
module.exports = async function dateReminder(db, bot) {
  // Выбираем неотработанные напоминания
  const records = await db.Record.find({
    isActive: true,
    isPeriodic: false,
    remindString: { $ne: null },
    isDone: false,
  }).limit(config.misc.dateReminderBulk); // Лимит 20, чтобы бота не забанили за спам

  const dateStr = moment(Date.now()).format('YYYY-MM-DD');
  records.forEach((record) => {
    // Обрабатываем только те, время которых наступило
    if (record.remindString < dateStr) {
      remindUser(bot, record);
    }
  });
};
