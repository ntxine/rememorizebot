const { Markup } = require('telegraf');
const { buttons } = require('../config/keyboards.js');

/**
 * Обработчик события callback_query
 * @param db
 * @param ctx
 * @returns {Promise<Message.TextMessage|ReturnType<Telegram[string]>|any>}
 */
module.exports = async function callbackQuery(db, ctx) {
  const { data } = ctx.update.callback_query;
  const parts = data.split('_');

  switch (parts[0]) {
    case 'done':
    case 'undone': {
      const record = await db.Record.findById(parseInt(parts[1]));
      if (!record) {
        return ctx.replyWithMarkdownV2('_Запись не найдена_');
      }
      record.isDone = parts[0] === 'done';
      await record.save();

      const keyboard = [[
        record.isDone
          ? Markup.button.callback(
          buttons.undone.text,
          `${buttons.undone.action}_${record._id}`,
          )
          : Markup.button.callback(
          buttons.done.text,
          `${buttons.done.action}_${record._id}`,
          ),
        Markup.button.callback(
          buttons.delete.text,
          `${buttons.delete.action}_${record._id}`
        ),
      ]];

      ctx.editMessageReplyMarkup({ inline_keyboard: keyboard });
      return ctx.answerCbQuery();
    }
    case 'delete': {
      await db.Record.findByIdAndDelete(parseInt(parts[1]));
      ctx.deleteMessage();
      return ctx.answerCbQuery();
    }
    default: break;
  }
};
