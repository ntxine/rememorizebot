const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const config = require('../config/index');

const userModel = require('./models/users');
const recordModel = require('./models/records');

let connection = null;

module.exports = {
  /**
   * Подключение к БД
   * @returns {Promise<void>}
   */
  connectDB: async () => {
    // Создаем подключение к БД MongoDB.
    connection = await mongoose.createConnection(config.database.uri, config.database.options);

    // Инициализируем модуль autoIncrement.
    autoIncrement.initialize(connection);

    // Создаем модели в ОРМ Mongoose
    module.exports.User = userModel(connection, autoIncrement.plugin);
    module.exports.Record = recordModel(connection, autoIncrement.plugin);

    console.log('DB connected')
  },

  /**
   * Отключение от БД
   * @returns {Promise<void>}
   */
  disconnectDB: async() => {
    if (connection) {
      connection.close();
      console.log('DB disconnected');
    }
  },
  connection,
};
