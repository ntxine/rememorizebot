const mongoose = require('mongoose');

/**
 * Модель пользователя
 * @type {*}
 */
const Schema = mongoose.Schema({
  telegramId: { type: Number, required: true },
  firstName: { type: String, required: true },
  username: { type: String, default: null },
  isBot: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
});

module.exports = (connection, autoIncrementPlugin) => {
  Schema.plugin(autoIncrementPlugin, { model: 'User', startAt: 1000000 });
  return connection.model('User', Schema);
};