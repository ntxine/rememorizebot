const mongoose = require('mongoose');

/**
 * Модель "Запись", для хранения в базе заметок и напоминаний
 * @type {*}
 */
const Schema = mongoose.Schema({
  telegramUserId: { type: Number, required: true },
  text: { type: String, required: true },
  isPeriodic: { type: Boolean, required: true },
  remindString: { type: String, default: null },
  isDone: { type: Boolean, default: false },
  isActive: { type: Boolean, default: true },
  createdAt: { type: Date, default: Date.now },
});

module.exports = (connection, autoIncrementPlugin) => {
  Schema.plugin(autoIncrementPlugin, { model: 'Record', startAt: 1000000 });
  return connection.model('Record', Schema);
};