// const util = require('util');
const { Telegraf } = require('telegraf');
const config = require('./config/index.js');
const db = require('./db');
const commands = require('./commands');
const events = require('./events');
const actions = require('./actions');
const timers = require('./timers');
const { buttons } = require('./config/keyboards.js');

/**
 * Инициализация бота
 * @type {Telegraf<Context>}
 */
const bot = new Telegraf(config.telegram.token);
// const depth = 4;

/**
 * Middleware, игнорирующий старые сообщения, необработанные пока бот не работал
 */
bot.use(async (ctx, next) => {
  const message = ctx.update.message || ctx.update.channel_post;
  if (message && message.date < config.telegram.botStartedAt) {
    console.log('dropping old message');
    return;
  }
  // console.log(
  //   util.inspect(ctx.update, { depth }),
  //   util.inspect(ctx.state, { depth })
  // );
  await next();
});

/**
 * Команды, начинающиеся с '/', для которых есть кнопки на основной клавиатуре
 */
Object.keys(buttons).forEach((key) => {
  bot.hears(buttons[key].text, ctx => commands[key](db, ctx));
});

/**
 * Колбеки, вызванные нажатием на inline-клавиатурах в поле сообщений
 */
Object.keys(actions).forEach((action) => {
  bot.action(action, ctx => actions[action](db, ctx));
});
bot.on('callback_query', ctx => actions.callbackQuery(db, ctx));

/**
 * Остальные события (стикер для примера, сообщения и общие команды)
 */
bot.help(ctx => commands.help(db, ctx));
bot.start(ctx => commands.start(db, ctx));
bot.on('sticker', ctx => events.sticker(db, ctx));
bot.on('message', ctx => events.message(db, ctx));

/**
 * Основной блок старта
 */
db.connectDB()
  .then(() => {
    // Запускаем бота
    bot.launch();
    // Ставим таймер на уведомления о событиях
    setInterval(() => timers.dateReminder(db, bot), config.misc.dateReminderInterval);
  })
  .catch((error) => {
    db.disconnectDB();
    // Корректная остановка бота при потере соединения с базой
    process.once('SIGINT', () => bot.stop('SIGINT'));
    process.once('SIGTERM', () => bot.stop('SIGTERM'));
    console.log('DB disconnected', error.message);
  });

// Время старта
console.log('Started at', Date(config.telegram.botStartedAt).toString());

// Корректная остановка бота при выгрузке приложения
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
