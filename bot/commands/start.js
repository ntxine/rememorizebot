const { keyboards } = require('../config/keyboards.js');

module.exports = async function start(db, ctx) {
  // Отбрасываем все попытки командовать ботом в группе
  if (ctx.message.chat.type !== 'private') {
    return;
  }

  // Получаем пользователя из базы, или создаем нового, если не найден
  const user = (await db.User.findOne({ telegramId: ctx.message.from.id })) || new db.User({
    telegramId: ctx.message.from.id,
    firstName: ctx.message.from.first_name || 'Анон',
    username: ctx.message.from.username || 'anonymous',
    isBot: ctx.message.from.is_bot,
  });

  const message = user.isNew
    ? `Приветстую, ${ctx.update.message.from.first_name}!`
    : `С возвращением, ${ctx.update.message.from.first_name}!`;

  return Promise.all([
    ctx.reply(message, keyboards.default.resize()),
    user.save(),
  ]);
};
