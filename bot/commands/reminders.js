module.exports = async function reminders(db, ctx) {
  const { id: telegramUserId } = ctx.update.message.from;

  // Выборка всех активных напоминания пользователя (по присутствию строки с датой напоминания)
  const records = await db.Record
    .find({ telegramUserId, isActive: true, remindString: { $ne: null } })
    .sort({ createdAt: 1 });

  if (!(records && records.length)) {
    return ctx.replyWithMarkdownV2('_Пока нет напоминаний_');
  }

  // Необходимо для синхронизации асинхронных вызовов reply
  for(let i = 0; i < records.length; i++) {
    const icon = records[i].isPeriodic ? '⏱' : '📅';
    await ctx.replyWithMarkdown(
      `${icon} _${records[i].remindString}_\n${records[i].text}`
    );
  }
};
