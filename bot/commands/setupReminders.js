const { Markup } = require('telegraf');
const { buttons } = require('../config/keyboards.js');

module.exports = async function setupNotes(db, ctx) {
  const { id: telegramUserId } = ctx.update.message.from;

  // Выбираем все напоминания для редактирования
  const records = await db.Record
    .find({ telegramUserId, isActive: true, remindString: { $ne: null } })
    .sort({ createdAt: 1 });
  if (!(records && records.length)) {
    return ctx.replyWithMarkdownV2('_Пока нет напоминаний_');
  }

  // Необходимо для синхронизации асинхронных вызовов reply
  for(let i = 0; i < records.length; i++) {
    const icon = records[i].isPeriodic ? '⏱' : '📅';
    const keyboard = Markup.inlineKeyboard([
      Markup.button.callback(
        buttons.delete.text,
        `${buttons.delete.action}_${records[i]._id}`
      ),
    ]);
    await ctx.replyWithMarkdown(
      `${icon} _${records[i].remindString}_\n${records[i].text}`,
      keyboard,
    );
  }
};
