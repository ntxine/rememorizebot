const { keyboards } = require('../config/keyboards.js');

module.exports = async function setup(db, ctx) {
  // При выходе из редактирования заменяем клавиатуру на стандартную
  ctx.reply('Главный раздел', keyboards.default.resize());
};
