const { keyboards } = require('../config/keyboards.js');

module.exports = async function setup(db, ctx) {
  // При переключении в режим редактирования заменяем клавиатуру
  ctx.reply('Управление записями', keyboards.setup.resize());
};
