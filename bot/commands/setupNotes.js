const { Markup } = require('telegraf');
const { buttons } = require('../config/keyboards.js');

module.exports = async function setupNotes(db, ctx) {
  const { id: telegramUserId } = ctx.update.message.from;

  // Выбираем все заметки для редактирования
  const records = await db.Record
    .find({ telegramUserId, isActive: true, remindString: null })
    .sort({ createdAt: 1 });

  if (!(records && records.length)) {
    return ctx.replyWithMarkdownV2('_Пока нет заметок_');
  }

  // Необходимо для синхронизации асинхронных вызовов reply
  for(let i = 0; i < records.length; i++) {
    const keyboard = Markup.inlineKeyboard([
      records[i].isDone
        ? Markup.button.callback(
          buttons.undone.text,
          `${buttons.undone.action}_${records[i]._id}`,
        )
        : Markup.button.callback(
          buttons.done.text,
          `${buttons.done.action}_${records[i]._id}`,
        ),

      Markup.button.callback(
        buttons.delete.text,
        `${buttons.delete.action}_${records[i]._id}`
      ),
    ]);
    await ctx.reply(records[i].text, keyboard);
  }
};
