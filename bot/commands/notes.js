module.exports = async function notes(db, ctx) {
  const { id: telegramUserId } = ctx.update.message.from;

  // Выборка всех активных заметок пользователя (по признаку отсутствия строки с датой напоминания)
  const records = await db.Record
    .find({ telegramUserId, isActive: true, remindString: null })
    .sort({ createdAt: 1 });

  if (!(records && records.length)) {
    return ctx.replyWithMarkdownV2('_Пока нет заметок_');
  }

  // for необходим для синхронизации асинхронных вызовов reply
  // forEach для этого не подходит
  for (let i = 0; i < records.length; i++) {
    await ctx.reply(records[i].isDone ? '✅ ' + records[i].text : records[i].text);
  }
};
