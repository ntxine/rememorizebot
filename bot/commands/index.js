module.exports = {
  start: require('./start.js'),
  help: require('./help.js'), // Справка
  notes: require('./notes.js'), // Все заметки
  reminders: require('./reminders.js'), // Все напоминания
  setup: require('./setup.js'), // Переключение на управление заметками/напоминаниями
  setupNotes: require('./setupNotes.js'), // Настройка заметок
  setupReminders: require('./setupReminders.js'), // Настройка напоминаний
  setupBack: require('./setupBack.js'), // Возврат в основной раздел
};